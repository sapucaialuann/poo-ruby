require_relative "person.rb"
class Student < Person
    #initialize
    def initialize cpf, address, name, age, birthdate, to_estudando = false, matricula
        super cpf, address, name, age, birthdate
        @matricula = matricula
        @to_estudando = to_estudando
    end
    
    def locomover
        @to_estudando = true
    end

    def to_estudando?
        if @to_estudando == true
            @activity = 'estudando'
        end
    end
    #certo?
    def presentation
        yield @cpf, @address, @name, @age, @birthdate, @matricula, @activity
    end
end

estudante = Student.new "xxx-xxx-xxx-xx", "icarai", "Ronaldo", "12", "14/03/2006", true, "1010"

estudante.presentation do |cpf, address, name, age, birthdate, matricula, activity|
    puts "\n\n"
    puts cpf, address, name, age, birthdate, matricula
    puts "\n\n"
    puts activity
end
estudante.to_estudando? do
    puts @activity
end


