require_relative "person.rb"
class Teacher < Person
    #initialize
    def initialize cpf, address, name, age, birthdate, subject = ["math"], formation = ['computer science']
        super cpf, address, name, age, birthdate
        @subject = subject
        @formation = formation
    end

    def presentation
        yield @cpf, @address, @name, @age, @birthdate, @subject, @formation
    end
end
#incomplete
prof = Teacher.new "323223", "sf", "Aderbal", "19", "14/03/2000" 
prof.presentation do |cpf, address, name, age, birthdate, subject, formation|
    puts "\n\n"
    puts cpf, address, name, age, birthdate, subject, formation
end